package contexts

import (
	"bitbucket.org/zaphome/sharedapi/adapters"
	"bitbucket.org/zaphome/sharedapi/context"
)

var adapterContexts = map[int]*context.Context{}

func AddAdapterContext(adapter *adapters.Adapter, context *context.Context) {
	adapterContexts[adapter.Id] = context
}

func GetAdapterContext(adapter *adapters.Adapter) *context.Context {
	return adapterContexts[adapter.Id]
}
