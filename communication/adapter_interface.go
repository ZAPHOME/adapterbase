package communication

import (
	"bitbucket.org/zaphome/sharedapi/logging"
	"bitbucket.org/zaphome/sharedapi/context"
	"bitbucket.org/zaphome/sharedapi/adapters"
	"bitbucket.org/zaphome/sharedapi/messaging"
	"reflect"
	"bitbucket.org/zaphome/sharedapi/messaging/communication"
)

func InitializeAdapterInterface(adapter *adapters.Adapter, c *context.Context) {
	logger := (*c.GetService("Logger")).(logging.Logger)
	defer logger.Info("Initialized adapter interface")
	AddHealthCheckObserver(adapter)
	AddDebugObserver(adapter, logger)
	adapter.MessageBus.DestinationChannel.StartObserving()
}

func AddHealthCheckObserver(adapter *adapters.Adapter) {
	// Handle HealthCheck Request message -> Send back a HealthCheck Response
	observer := communication.Observer(func (request messaging.Message) {
		hcRequest := request.(*adapters.AdapterHealthCheckRequest)
		adapter.MessageBus.SourceChannel.SendMessage(
			adapters.BuildAdapterHealthCheckResponse(adapter.Id, hcRequest.GetId()),
		)
	})

	// Add handler to message bus
	adapter.MessageBus.DestinationChannel.AddObserver(
		reflect.TypeOf(&adapters.AdapterHealthCheckRequest{}),
		&observer,
	)
}

func AddDebugObserver(adapter *adapters.Adapter, logger logging.Logger) {
	// Write each message to the log with Debug log level
	processMessage := func (message messaging.Message) {
		logger.Debug("Received message: %#v", message)
	}

	// Add the debug observer to the global observer
	observer := communication.Observer(processMessage)
	adapter.MessageBus.DestinationChannel.AddGlobalObserver(&observer)
}
