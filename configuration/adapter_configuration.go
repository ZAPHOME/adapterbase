package configuration

import (
	"bitbucket.org/zaphome/sharedapi/logging"
	"bitbucket.org/zaphome/sharedapi/context"
	"bitbucket.org/zaphome/sharedapi/adapters"
	"bitbucket.org/zaphome/adapterbase/communication"
	"bitbucket.org/zaphome/adapterbase/contexts"
)

func Init(adapter *adapters.Adapter) (c *context.Context) {
	c = createContext(adapter)
	logger := createLogger(adapter, c)
	communication.InitializeAdapterInterface(adapter, c)
	logger.Info("Loaded Adapter: %s", adapter.Name)
	return
}

func createContext(adapter *adapters.Adapter) (c *context.Context) {
	c = context.CreateEmptyContext()
	c.AddServiceWithTypeName(*adapter)
	contexts.AddAdapterContext(adapter, c)
	return
}

func createLogger(adapter *adapters.Adapter, c *context.Context) (logger *logging.Logger) {
	logger = logging.CreateConsoleLogger(adapter.Name)
	c.AddServiceWithTypeName(*logger)
	return
}



